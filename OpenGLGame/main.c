#ifdef WIN32
#define WINx32
#endif
#include "main.h"

Cell fields[30][16];
enum SmileState currentSmileState = IDLE;
enum SettingsState currentSettingsState = CLOSE;
int isGame = 0, isPregame = 1;
int prevClick[2] = {0, 0};
int looseMine[2] = {0, 0};
int SettingsOKState = 0, SettingsCancelState = 0;
int currentMode = 5;
int radioButtonChecked = 0;
int SettingsIsOpen = 0;

int WIDTH = 30;
int HEIGHT = 16;
int MINESCOUNT = 99;
int flagsCount = 99;
int timerValue = 0;

void randomizeMines(int count)
{
	int x, y, i;
    isPregame = 1;
    isGame = 1;
    flagsCount = MINESCOUNT;
    timerValue = 0;
    looseMine[0] = 0;
    looseMine[1] = 0;

    for (x = 0; x < WIDTH; x++)
    {
        for (y = 0; y < HEIGHT; y++)
        {
            fields[x][y].hasMine = 0;
            fields[x][y].state = CLOSED;
        }
    }
    
    for (i = 0; i < count; ++i)
    {
        do
        {
            x = rand() % WIDTH;
            y = rand() % HEIGHT;
        }
        while (fields[x][y].hasMine);
        
        fields[x][y].hasMine = 1;
    }
}

void SetDefaultMode()
{
    if (currentMode == 5)
    {
        WIDTH = 8;
        HEIGHT = 8;
        MINESCOUNT = 10;
        flagsCount = MINESCOUNT;
    }
    currentMode = 0;
}

void initialization()
{
#ifdef WINx32
    SetDefaultMode();
#endif
    glClearColor(1, 1, 1, 0.0f);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, WIDTH * CELLSIZE + OFFSETS[0] + OFFSETS[1], 0, HEIGHT * CELLSIZE + OFFSETS[2] + OFFSETS[3] + GLOBALOFFSET, -5.0, 5.0);
    //srand(time(0));
#ifndef WINx32
    SetDefaultMode();
#endif
    randomizeMines(MINESCOUNT);
}

void reshape(int width, int height) {
    glViewport(0,0,width,height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, width, 0, height);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void Win()
{
    printf("You won!");
    isGame = 0;
    currentSmileState = WIN;
    glutPostRedisplay();
}

void Loose()
{
    int i, j;
    isGame = 0;
    for (i = 0; i < WIDTH; i++)
    {
        for (j = 0; j < HEIGHT; j++)
        {
            if (fields[i][j].state == CLOSED && fields[i][j].hasMine)
                fields[i][j].state = OPENED;
        }
    }
    currentSmileState = LOSE;
    glutPostRedisplay();
}
void checkWin()
{
    int tmp = 0;
    int x, y;
    
    if (flagsCount == 0 && isGame)
    {
        for (x = 0; x < WIDTH; x++)
        {
            for (y = 0; y < HEIGHT; y++)
            {
                
                if (fields[x][y].state == CLOSED || fields[x][y].state == CLICKED)
                    tmp++;
            }
        }
        if (tmp == 0)
            Win();
    }
}

int getMinesAround(int i, int j)
{
    int minesAround = 0;
	int x, y;

    for (y = j - 1; y <= j + 1; ++y)
        for (x = i - 1; x <= i + 1; ++x)
        {
            if ((x == i && y == j) ||
                x < 0 || x >= WIDTH ||
                y < 0 || y >= HEIGHT)
                continue;
            if (fields[x][y].hasMine == 1)
                ++minesAround;
        }
    return  minesAround;
}

void checkFieldsStatus()
{
    int i, j;
    int y, x;
    
    for (i = 0; i < WIDTH; i++)
    {
        for (j = 0; j < HEIGHT; j++)
        {
            switch (fields[i][j].state)
            {
                case CLICKED:
                    drawClosedField(i, j, 1);
                    break;
                case CLOSED:
                    drawClosedField(i, j, 0);
                    break;
                case OPENED:
                    isPregame = 0;
                    if (fields[i][j].hasMine == 0)
                    {
                        if (getMinesAround(i,j) == 0)
                        {
                            for (y = j - 1; y <= j + 1; ++y)
                                for (x = i - 1; x <= i + 1; ++x)
                                {
                                    if ((x == i && y == j) ||
                                        x < 0 || x >= WIDTH ||
                                        y < 0 || y >= HEIGHT || fields[x][y].state == FLAG)
                                        continue;
                                    fields[x][y].state = OPENED;
                                    drawOpenedFieldWithValues(x, y, getMinesAround(x,y));
                                    glutPostRedisplay();
                                }
                        }
                        drawOpenedFieldWithValues(i, j, getMinesAround(i,j));
                    } else
                    {
                        if (isGame)
                        {
                            drawMine(i, j, 1);
                            looseMine[0] = i;
                            looseMine[1] = j;
                        }
                        
                        else
                        {
                            drawMine(i, j, 0);
                            drawMine(looseMine[0], looseMine[1], 1);
                        }
                        Loose();
                    }
                    break;
                case FLAG:
                    isPregame = 0;
                    if (!isGame && fields[i][j].hasMine == 0)
                    {
                        drawCellWithFlag(i, j, 1);
                    } else
                        drawCellWithFlag(i, j, 0);
                    break;
            }
        }
    }
}


void draw()
{
    glClear(GL_COLOR_BUFFER_BIT);
    
    drawBackground();
    checkFieldsStatus();
    DrawSmileButton(currentSmileState);
    drawFlagsCount();
    checkWin();
    drawMenuBar(currentSettingsState);
    
    glutSwapBuffers();
}

int getFlagsAround(int i, int j)
{
    int flagsAround = 0;
	int x, y;

    for (y = j - 1; y <= j + 1; ++y)
        for (x = i - 1; x <= i + 1; ++x)
        {
            if ((x == i && y == j) ||
                x < 0 || x >= WIDTH ||
                y < 0 || y >= HEIGHT)
                continue;
            if (fields[x][y].state == FLAG)
                ++flagsAround;
        }
    return  flagsAround;
}

void openOtherFields()
{
    int i = prevClick[0];
    int j = prevClick[1];
	int x, y;
    
    for (y = j - 1; y <= j + 1; ++y)
        for (x = i - 1; x <= i + 1; ++x)
        {
            if ((x == i && y == j) ||
                x < 0 || x >= WIDTH ||
                y < 0 || y >= HEIGHT || fields[x][y].state == FLAG)
                continue;
            if (fields[x][y].state == CLOSED)
            {
                fields[x][y].state = OPENED;
                glutPostRedisplay();
            }
        }
                
}

void ClickSmileButton()
{
    randomizeMines(MINESCOUNT);
    currentSmileState = IDLE;
    isPregame = 1;
    glutPostRedisplay();
}

int CheckZone(int x, int y)
{
    int x1, x2, y1, y2;
    
    x1 = OFFSETS[0];
    x2 = WIDTH * CELLSIZE + OFFSETS[0];
    y1 = OFFSETS[3] + GLOBALOFFSET;
    y2 = HEIGHT * CELLSIZE + OFFSETS[3] + GLOBALOFFSET;
    
    if (x > x1  && x < x2)
    {
        if (y > y1 && y < y2)
        {
            return 1;
        }
        else return 0;
    }
    else return 0;
}

void CheckSettings(int x, int y, int clicked)
{
	int SettingsWidowID = glutGetWindow();
    if (x > 20 - 6 && x < 20 + 6 && y > 200 - 180 - 6 && y < 200 - 180 + 6)
        radioButtonChecked = 0;
    if (x > 20 - 6 && x < 20 + 6 && y > 200 - 155 - 6 && y < 200 - 155 + 6)
        radioButtonChecked = 1;
    if (x > 20 - 6 && x < 20 + 6 && y > 200 - 130 - 6 && y < 200 - 130 + 6)
        radioButtonChecked = 2;
    
    if (clicked)
    {
        SettingsOKState = 0;
        SettingsCancelState = 0;
        
        if (x > 10 && x < 10 + 70 && y > 200 - 10 - 30 && y < 200 - 10)
        {
            currentMode = radioButtonChecked;
            
            switch (radioButtonChecked) {
                case 0:
                    WIDTH = 8;
                    HEIGHT = 8;
                    MINESCOUNT = 10;
                    flagsCount = MINESCOUNT;
                    break;
                case 1:
                    WIDTH = 16;
                    HEIGHT = 16;
                    MINESCOUNT = 40;
                    flagsCount = MINESCOUNT;
                    break;
                case 2:
                    WIDTH = 30;
                    HEIGHT = 16;
                    MINESCOUNT = 99;
                    flagsCount = MINESCOUNT;
                    break;
                default:
                    break;
            }
			glutSetWindow(1);
			printf("Mode changed to %d, settings windows have id: %d\n", currentMode, glutGetWindow());
            SettingsIsOpen = 0;
            glutReshapeWindow(WIDTH * CELLSIZE + OFFSETS[0] + OFFSETS[1], HEIGHT * CELLSIZE  + OFFSETS[2] + OFFSETS[3] + GLOBALOFFSET);
#ifdef WIN32
			initialization();
#endif
            randomizeMines(MINESCOUNT);
			glutDestroyWindow(SettingsWidowID);
        }
        if (x > 100 && x < 100 + 90 && y > 200 - 10 - 30 && y < 200 - 10)
        {
            SettingsIsOpen = 0;
            glutDestroyWindow(SettingsWidowID);
        }
    } else
    {
        if (x > 10 && x < 10 + 70 && y > 200 - 10 - 30 && y < 200 - 10)
        {
            SettingsOKState = 1;
        }
        if (x > 100 && x < 100 + 90 && y > 200 - 10 - 30 && y < 200 - 10)
        {
            SettingsCancelState = 1;
        }
    }
    glutPostRedisplay();
}

void mouse2(int button, int state, int x, int y)
{
    CheckSettings(x, y, (state == GLUT_UP) ? 1 : 0);
}


void initializationSettings()
{
    glClearColor(1, 1, 1, 0.0f);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, 200, 0, 200, -5.0, 5.0);
    glutMouseFunc(mouse2);
}

void drawSettings()
{
    glClear(GL_COLOR_BUFFER_BIT);
            
    DrawRadioBox(20, 180, (radioButtonChecked == 0) ? 1 : 0);
    DrawRadioBox(20, 155, (radioButtonChecked == 1) ? 1 : 0);
    DrawRadioBox(20, 130, (radioButtonChecked == 2) ? 1 : 0);
    
    Sprint(40, 175, "Beginner", GLUT_BITMAP_8_BY_13);
    Sprint(40, 150, "Intermediate", GLUT_BITMAP_8_BY_13);
    Sprint(40, 125, "Proffesional", GLUT_BITMAP_8_BY_13);
    
    DrawButton(10, 10, 70, 30, 3, "OK", 10, 6, SettingsOKState);
    DrawButton(100, 10, 90, 30, 3, "Cancel", 24, 6, SettingsCancelState);
    
    glutSwapBuffers();
}

void OpenSettings()
{
     int x, y;
    if (SettingsIsOpen)
        return;
    SettingsIsOpen = 1;
   
    //WIDTH * CELLSIZE + OFFSETS[0] + OFFSETS[1], HEIGHT * CELLSIZE  + OFFSETS[2] + OFFSETS[3] + GLOBALOFFSET
    x = glutGet((GLenum)GLUT_WINDOW_X);
    y = glutGet((GLenum)GLUT_WINDOW_Y);
    glutInitWindowPosition (x + (WIDTH * CELLSIZE + OFFSETS[0] + OFFSETS[1]) / 2 - 100, y + (HEIGHT * CELLSIZE  + OFFSETS[2] + OFFSETS[3] + GLOBALOFFSET) / 2 - 100);
    glutCreateWindow ("Settings");
    //glutCreateSubWindow(5, x + (WIDTH * CELLSIZE + OFFSETS[0] + OFFSETS[1]) / 2 - 100, y + (HEIGHT * CELLSIZE  + OFFSETS[2] + OFFSETS[3] + GLOBALOFFSET) / 2 - 100, 200, 200);
    glutReshapeWindow(200, 200);
    glutDisplayFunc(drawSettings);
    radioButtonChecked = currentMode;
    currentSettingsState = OPEN;
    initializationSettings();
}

void ResetState()
{
	int i, j;
    currentSettingsState = CLOSE;
    for (i = 0; i < WIDTH; i++)
        for (j = 0; j < HEIGHT; j++)
            if (fields[i][j].state == CLICKED)
                fields[i][j].state = CLOSED;
}


void mouse(int button, int state, int x, int y)
{
    //printf("x: %d y: %d\n", x, y);

    float SmileCenterX = (WIDTH * CELLSIZE + OFFSETS[0] + OFFSETS[1]) / 2, SmileCenterY = CELLSIZE * HEIGHT + OFFSETS[3] - GLOBALOFFSET - (((CELLSIZE * HEIGHT + OFFSETS[3]) - (CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2])))) / 2;
    int fieldX = (x - OFFSETS[0]) / CELLSIZE, fieldY = ((HEIGHT * CELLSIZE) - y + GLOBALOFFSET + OFFSETS[3]) / CELLSIZE;
    
    if (state == GLUT_UP)
    {
        prevClick[0] = fieldX;
        prevClick[1] = fieldY;
        
        switch (button)
        {
            case GLUT_LEFT_BUTTON:
                if (x > 0 && x < CELLSIZE * 5 && y > 0 && y < GLOBALOFFSET)
                {
                    currentSettingsState = CLOSE;
                    glutPostRedisplay();
                    OpenSettings();
                    
                }
                if (x > SmileCenterX - 15 && x < SmileCenterX + 15 && (HEIGHT * CELLSIZE) - y + OFFSETS[3] > SmileCenterY - 30 && (HEIGHT * CELLSIZE) - y + OFFSETS[3] < SmileCenterY)
                    ClickSmileButton();
                else
                {
                    if (isGame)
                    {
                        if (fields[fieldX][fieldY].state == CLICKED)
                        {
                            fields[fieldX][fieldY].state = OPENED;
                        }
                        else if (fields[fieldX][fieldY].state == OPENED && (fieldX == prevClick[0] && fieldY == prevClick[1]))
                              {
                                  int mines = getMinesAround(fieldX, fieldY);
                                  int flags = getFlagsAround(fieldX, fieldY);
                                  if (mines == flags)
                                  {
                                      openOtherFields();
                                  }
                              }
                        currentSmileState = IDLE;
                        glutPostRedisplay();
                    }
                }
                break;
            case GLUT_RIGHT_BUTTON:
                if (isGame)
                {
                    if (fields[fieldX][fieldY].state == FLAG)
                    {
                        fields[fieldX][fieldY].state = CLOSED;
                        flagsCount++;
                    }
                    else if (fields[fieldX][fieldY].state == CLICKED)
                    {
                        fields[fieldX][fieldY].state = FLAG;
                        flagsCount--;
                    }
                    currentSmileState = IDLE;
                }
                break;
        }
         ResetState();
        glutPostRedisplay();
    } else
        if (button == GLUT_LEFT_BUTTON || button == GLUT_RIGHT_BUTTON)
        {
            if ((x > SmileCenterX - 15 && x < SmileCenterX + 15 && (HEIGHT * CELLSIZE) - y + OFFSETS[3] > SmileCenterY - 30 && (HEIGHT * CELLSIZE) - y + OFFSETS[3] < SmileCenterY) && button == GLUT_LEFT_BUTTON)
            {
                currentSmileState = DOWN;
                glutPostRedisplay();
            } else if (isGame)
            {
                if (fields[fieldX][fieldY].state == CLOSED && CheckZone(x, y))
                {
                    fields[fieldX][fieldY].state = CLICKED;
                    currentSmileState = WAITING;
                    glutPostRedisplay();
                    
                } else if (x > 0 && x < CELLSIZE * 5 && y > 0 && y < GLOBALOFFSET)
                {
                    currentSettingsState = OPEN;
                    glutPostRedisplay();
                }
            }
        }
}

void timer(int x)
{
    if (isGame && !isPregame)
        timerValue++;
    glutTimerFunc(1000,timer,0);
    glutPostRedisplay();
}

void resize(int width, int height) {
    glutReshapeWindow(WIDTH * CELLSIZE + OFFSETS[0] + OFFSETS[1], HEIGHT * CELLSIZE  + OFFSETS[2] + OFFSETS[3] + GLOBALOFFSET);
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize (WIDTH * CELLSIZE + OFFSETS[0] + OFFSETS[1], HEIGHT * CELLSIZE  + OFFSETS[2] + OFFSETS[3] + GLOBALOFFSET);
    glutInitWindowPosition (15, 250);
    glutCreateWindow ("Minesweeper");
    
    glutDisplayFunc(draw);
    glutMouseFunc(mouse);
    glutReshapeFunc(resize);
    glutTimerFunc(1000,timer,0);
    
    initialization();
    
    //OpenSettings();
    
    glutMainLoop();
    return 0;
}