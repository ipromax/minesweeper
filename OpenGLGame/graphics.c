#include "graphics.h"

void Sprint( int x, int y, char *st, void *font)
{
    int l,i;
    l=strlen(st);
    glRasterPos2i(x, y);
    for(i = 0; i < l; i++)
    {
        glutBitmapCharacter(font, st[i]);
    }   
}

void drawMenuBar(enum SettingsState state)
{
    //state = OPEN;
    glColor3f(0.75, 0.75, 0.75);
    
    //Background
    glBegin(GL_QUADS);
    glVertex2f(0, HEIGHT * CELLSIZE + OFFSETS[2] + OFFSETS[3]);
    glVertex2f(0, HEIGHT * CELLSIZE + OFFSETS[2] + OFFSETS[3] + GLOBALOFFSET);
    glVertex2f(WIDTH * CELLSIZE + OFFSETS[0] + OFFSETS[1], HEIGHT * CELLSIZE + OFFSETS[2] + OFFSETS[3] + GLOBALOFFSET);
    glVertex2f(WIDTH * CELLSIZE + OFFSETS[0] + OFFSETS[1], HEIGHT * CELLSIZE + OFFSETS[2] + OFFSETS[3]);
    glEnd();
    
    if (state == CLOSE)
        DrawButton(0, HEIGHT * CELLSIZE + OFFSETS[2] + OFFSETS[3], CELLSIZE * 5, GLOBALOFFSET, 3,"Settings", 35, 5, 0);
    else
        DrawButton(0, HEIGHT * CELLSIZE + OFFSETS[2] + OFFSETS[3], CELLSIZE * 5, GLOBALOFFSET, 3,"Settings", 35, 5, 1);
}

void DrawButton(int x, int y, int width, int height, int border, char *caption, int fontOffsetX, int fontOffsetY, int clicked)
{
    if (!clicked)
        glColor3f(1, 1, 1);
    else
        glColor3f(0.5, 0.5, 0.5);
    
    
    glBegin(GL_TRIANGLES);
    
    glVertex2f(x, y);
    glVertex2f(x, y + height);
    glVertex2f(x + height, y + height);
    
    glVertex2f(width + x - height, y);
    glVertex2f(width + x - height, y + height);
    glVertex2f(width + x, y + height);
    
     if (!clicked)
        glColor3f(0.5, 0.5, 0.5);
    else
        glColor3f(1, 1, 1);
    
    glVertex2f(x + height, y);
    glVertex2f(x + height, y + height);
    glVertex2f(x, y);
    
    glVertex2f(width + x - height, y);
    glVertex2f(width + x, y + height);
    glVertex2f(width + x, y);
    
    glEnd();

    glBegin(GL_QUADS);
    
     if (!clicked)
         glColor3f(1, 1, 1);
    else
        glColor3f(0.5, 0.5, 0.5);
    
    glVertex2f(x, y + height);
    glVertex2f(x - border + width, y + height);
    glVertex2f(x - border + width, y + height - border);
    glVertex2f(x, y + height - border);

     if (!clicked)
        glColor3f(0.5, 0.5, 0.5);
    else
        glColor3f(1, 1, 1);
    
    glVertex2f(x + border, y);
    glVertex2f(x + width - border, y);
    glVertex2f(x + width - border, y + border);
    glVertex2f(x + border, y + border);

    glColor3f(0.75, 0.75, 0.75);
    
    glVertex2f(x + border, y + border);
    glVertex2f(x + border, y + height - border);
    glVertex2f(x + width - border, y + height - border);
    glVertex2f(x + width - border, y + border);
    
    glEnd();
    
    glColor3f(0, 0, 0);
    
    Sprint(x + (width/2) - fontOffsetX, y + (height/2) - fontOffsetY, caption, GLUT_BITMAP_9_BY_15);

}

void drawFlagsCount()
{
    int x = CELLSIZE + OFFSETS[0], y = CELLSIZE * HEIGHT + OFFSETS[3] - (((CELLSIZE * HEIGHT + OFFSETS[3]) - (CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2])))) / 2 - 8;
    
    char buf[4];
    char tmp[4];

    glColor3f(0, 0, 0);
    glBegin(GL_QUADS);
    glVertex2f(x - 4, y - 8);
    glVertex2f(x + 40, y - 8);
    glVertex2f(x + 40, y + 24);
    glVertex2f(x - 4, y + 24);
    
    glVertex2f(CELLSIZE * WIDTH + OFFSETS[1] - 15, y - 8);
    glVertex2f(CELLSIZE * WIDTH + OFFSETS[1] - 15, y + 24);
    glVertex2f(CELLSIZE * WIDTH + OFFSETS[1] - 60, y + 24);
    glVertex2f(CELLSIZE * WIDTH + OFFSETS[1] - 60, y - 8);

    glEnd();
    glColor3f(1, 0, 0);

    
    sprintf(buf, "%d", timerValue);
    if (timerValue < 10)
    {
        strcpy(tmp, "00");
        strcat(tmp, buf);
    } else if (timerValue >= 10 && timerValue < 100)
    {
        strcpy(tmp, "0");
        strcat(tmp, buf);
    } else
    {
        strcpy(tmp, "");
        strcat(tmp, buf);
    }
    
    Sprint(x, y, tmp, GLUT_BITMAP_TIMES_ROMAN_24);
    
    sprintf(buf, "%d", flagsCount);
    if (flagsCount < 10)
    {
        strcpy(tmp, "00");
        strcat(tmp, buf);
    } else if (flagsCount >= 10 && flagsCount < 100)
    {
        strcpy(tmp, "0");
        strcat(tmp, buf);
    }
    Sprint(CELLSIZE * WIDTH + OFFSETS[0] + OFFSETS[1] - 71, y, tmp, GLUT_BITMAP_TIMES_ROMAN_24);

}

void DrawCircle(float cx, float cy, float r, int num_segments, int solid)
{
	 float angle = 0.0;
     float x = 0.0;
     float y = 0.0;
	 int i = 0;
    glBegin((!solid) ? GL_LINE_LOOP : GL_TRIANGLE_FAN);

    for(i = 0; i < num_segments; i++)
    {
        angle = 2.0f * 3.1415926f * i / num_segments;
        x = r * cosf(angle);
        y = r * sinf(angle);
        glVertex2f(x + cx, y + cy);
    }
    glEnd();
}

void DrawArc(float cx, float cy, float r, float start_angle, float arc_angle, int num_segments)
{
    float theta = arc_angle / 1.0 * (num_segments - 1);//theta is now calculated from the arc angle instead, the - 1 bit comes from the fact that the arc is open
    
    float tangetial_factor = tanf(theta);
    
    float radial_factor = cosf(theta);
    
    
    float x = r * cosf(start_angle);//we now start at the start angle
    float y = r * sinf(start_angle);
	int ii = 0;
	float tx = 0.0f;
    float ty = 0.0f;
    
    glBegin(GL_LINE_STRIP);//since the arc is not a closed curve, this is a strip now
    for(ii = 0; ii < num_segments; ii++)
    {
        glVertex2f(x + cx, y + cy);
        
        tx = -y;
        ty = x;
        
        x += tx * tangetial_factor;
        y += ty * tangetial_factor;
        
        x *= radial_factor;
        y *= radial_factor;
    }
    glEnd();
}

void DrawSimpleSmileButton(float defaultX, float defaultY, int down)
{
    if (down)
        glColor3f(1.0, 1.0, 1.0);
    else
        glColor3f(0.5, 0.5, 0.5);
    glBegin(GL_TRIANGLES);
    glVertex2f(defaultX - 15, defaultY - 15);
    glVertex2f(defaultX + 15, defaultY - 15);
    glVertex2f(defaultX + 15, defaultY + 15);
    
    if (down)
        glColor3f(0.5, 0.5, 0.5);
    else
        glColor3f(1, 1, 1);
    glVertex2f(defaultX - 15, defaultY - 15);
    glVertex2f(defaultX - 15, defaultY + 15);
    glVertex2f(defaultX + 15, defaultY + 15);
    glEnd();
    
    glColor3f(0.756, 0.756, 0.756);
    glBegin(GL_QUADS);
    glVertex2f(defaultX - 15 + 3, defaultY - 15 + 3);
    glVertex2f(defaultX - 15 + 3, defaultY + 15 - 3);
    glVertex2f(defaultX + 15 - 3, defaultY + 15 - 3);
    glVertex2f(defaultX + 15 - 3, defaultY - 15 + 3);
    glEnd();
}

void DrawSmileButton(enum SmileState state)
{
    float defaultX = (WIDTH * CELLSIZE + OFFSETS[0] + OFFSETS[1]) / 2, defaultY = CELLSIZE * HEIGHT + OFFSETS[3] - (((CELLSIZE * HEIGHT + OFFSETS[3]) - (CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2])))) / 2;
    glLineWidth(1.0);
    switch (state) {
        case WIN:
            DrawSimpleSmileButton(defaultX, defaultY, 0);
            glColor3f(1, 1, 0);
            DrawCircle(defaultX, defaultY, 10, 10, 1);
            glColor3f(0, 0, 0);
            DrawCircle(defaultX, defaultY, 10, 10, 0);
            DrawCircle(defaultX - 3, defaultY + 2, 3, 6, 1);
            DrawCircle(defaultX + 3, defaultY + 2, 3, 6, 1);
            DrawArc(defaultX, defaultY, 6, -19.5,  -40, 12);
            glBegin(GL_LINES);
            glVertex2f(defaultX - 10, defaultY);
            glVertex2f(defaultX - 3, defaultY + 2);
            
            glVertex2f(defaultX + 10, defaultY);
            glVertex2f(defaultX + 3, defaultY + 2);
            
            glVertex2f(defaultX - 3, defaultY + 3.95);
            glVertex2f(defaultX + 3, defaultY + 3.95);
            glEnd();
            break;
        case LOSE:
            DrawSimpleSmileButton(defaultX, defaultY, 0);
            glColor3f(1, 1, 0);
            DrawCircle(defaultX, defaultY, 10, 10, 1);
            glColor3f(0, 0, 0);
            DrawCircle(defaultX, defaultY, 10, 10, 0);
            
            glBegin(GL_LINES);
            glVertex2f(defaultX - 5, defaultY + 4);
            glVertex2f(defaultX - 1, defaultY);
            glVertex2f(defaultX - 5, defaultY);
            glVertex2f(defaultX - 1, defaultY + 4);
            
            glVertex2f(defaultX + 5, defaultY + 4);
            glVertex2f(defaultX + 1, defaultY);
            glVertex2f(defaultX + 5, defaultY);
            glVertex2f(defaultX + 1, defaultY + 4);
            glEnd();
            DrawArc(defaultX, defaultY - 8, 6, 19.5,  40, 12);
            break;
        case IDLE:
            DrawSimpleSmileButton(defaultX, defaultY, 0);
            glColor3f(1, 1, 0);
            DrawCircle(defaultX, defaultY, 10, 10, 1);
            glColor3f(0, 0, 0);
            DrawCircle(defaultX, defaultY, 10, 10, 0);
            DrawCircle(defaultX - 3, defaultY + 2, 2, 4, 1);
            DrawCircle(defaultX + 3, defaultY + 2, 2, 4, 1);
            DrawArc(defaultX, defaultY, 6, -19.5,  -40, 12);
            break;
        case DOWN:
            DrawSimpleSmileButton(defaultX, defaultY, 1);
            glColor3f(1, 1, 0);
            DrawCircle(defaultX, defaultY, 10, 10, 1);
            glColor3f(0, 0, 0);
            DrawCircle(defaultX, defaultY, 10, 10, 0);
            DrawCircle(defaultX - 3, defaultY + 2, 2, 4, 1);
            DrawCircle(defaultX + 3, defaultY + 2, 2, 4, 1);
            DrawArc(defaultX, defaultY, 6, -19.5,  -40, 12);
            break;
        case WAITING:
            DrawSimpleSmileButton(defaultX, defaultY, 0);
            glColor3f(1, 1, 0);
            DrawCircle(defaultX, defaultY, 10, 10, 1);
            glColor3f(0, 0, 0);
            DrawCircle(defaultX, defaultY, 10, 10, 0);
            DrawCircle(defaultX - 3, defaultY + 2, 2, 4, 1);
            DrawCircle(defaultX + 3, defaultY + 2, 2, 4, 1);
            DrawCircle(defaultX, defaultY - 4, 3, 10, 0);
            break;
            break;
        default:
            break;
    }
}

void drawBackground()
{
    glColor3f(0.5, 0.5, 0.5);
    glBegin(GL_TRIANGLES);
    glVertex2f(0, 0);
    glVertex2f(WIDTH * CELLSIZE + OFFSETS[0] + OFFSETS[1], 0);
    glVertex2f(WIDTH * CELLSIZE + OFFSETS[0] + OFFSETS[1], HEIGHT * CELLSIZE  + OFFSETS[2] + OFFSETS[3]);
    glEnd();
    
    
    glColor3f(0.756, 0.756, 0.756);
    glBegin(GL_QUADS);
    glVertex2f(3, 3);
    glVertex2f(WIDTH * CELLSIZE + OFFSETS[0] + OFFSETS[1] - 3, 3);
    glVertex2f(WIDTH * CELLSIZE + OFFSETS[0] + OFFSETS[1] - 3, HEIGHT * CELLSIZE + OFFSETS[2] + OFFSETS[3] - 3);
    glVertex2f(3, HEIGHT * CELLSIZE + OFFSETS[2] + OFFSETS[3] - 3);
    glEnd();
    
    glColor3f(1, 1, 1);
    glBegin(GL_TRIANGLES);
    glVertex2f(OFFSETS[0] - 3, OFFSETS[2] - 3);
    glVertex2f(CELLSIZE * WIDTH + (OFFSETS[0] + 3), OFFSETS[2] - 3);
    glVertex2f(CELLSIZE * WIDTH + (OFFSETS[0] + 3), CELLSIZE * HEIGHT + (OFFSETS[2] + 3));
    
    glColor3f(0.5, 0.5, 0.5);
    glVertex2f(OFFSETS[0] - 3, OFFSETS[2] - 3);
    glVertex2f(OFFSETS[0] - 3, CELLSIZE * HEIGHT + (OFFSETS[2] + 3));
    glVertex2f(CELLSIZE * WIDTH + (OFFSETS[0] + 3), CELLSIZE * HEIGHT + (OFFSETS[2] + 3));
    
    glColor3f(1, 1, 1);
    //     *
    //   *** White (Right)
    // *****
    glVertex2f(CELLSIZE * WIDTH + (OFFSETS[0] + 3) - ((CELLSIZE * HEIGHT + OFFSETS[3] + 3) - (CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2] - 3))), CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2] - 3));
    glVertex2f(CELLSIZE * WIDTH + (OFFSETS[0] + 3), CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2] - 3));
    glVertex2f(CELLSIZE * WIDTH + (OFFSETS[0] + 3), CELLSIZE * HEIGHT + OFFSETS[3] + 3);
    //     *
    //   *** White (Left)
    // *****
    glVertex2f(OFFSETS[0] - 3, CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2] - 3));
    glVertex2f(OFFSETS[0] - 3 + (CELLSIZE * HEIGHT + OFFSETS[3] + 3) - (CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2] - 3)), CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2] - 3));
    glVertex2f(OFFSETS[0] - 3 + (CELLSIZE * HEIGHT + OFFSETS[3] + 3) - (CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2] - 3)), CELLSIZE * HEIGHT + OFFSETS[3] + 3);
    
    glColor3f(0.5, 0.5, 0.5);
    // *****
    // ***  Gray (Left)
    // *
    glVertex2f(OFFSETS[0] - 3, CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2] - 3));
    glVertex2f(OFFSETS[0] - 3 + (CELLSIZE * HEIGHT + OFFSETS[3] + 3) - (CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2] - 3)), CELLSIZE * HEIGHT + OFFSETS[3] + 3);
    glVertex2f(OFFSETS[0] - 3, CELLSIZE * HEIGHT + OFFSETS[3] + 3);
    
    // *****
    // ***  Gray (Right)
    // *
    glVertex2f(CELLSIZE * WIDTH + (OFFSETS[0] + 3) - ((CELLSIZE * HEIGHT + OFFSETS[3] + 3) - (CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2] - 3))), CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2] - 3));
    glVertex2f(CELLSIZE * WIDTH + (OFFSETS[0] + 3), CELLSIZE * HEIGHT + OFFSETS[3] + 3);
    glVertex2f(CELLSIZE * WIDTH + (OFFSETS[0] + 3) - ((CELLSIZE * HEIGHT + OFFSETS[3] + 3) - (CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2] - 3))), CELLSIZE * HEIGHT + OFFSETS[3] + 3);
    glEnd();
    
    
    glBegin(GL_QUADS);
    glColor3f(1, 1, 1);
    glVertex2f(OFFSETS[0] - 3 + (CELLSIZE * HEIGHT + OFFSETS[3] - 3) - (CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2] - 3)), CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2] + 3));
    glVertex2f(OFFSETS[0] - 3 + (CELLSIZE * HEIGHT + OFFSETS[3] - 3) - (CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2] - 3)), CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2] - 3));
    glVertex2f(CELLSIZE * WIDTH + (OFFSETS[0] + 3), CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2] - 3));
    glVertex2f(CELLSIZE * WIDTH + (OFFSETS[0] + 3), CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2] + 3));
    
    glColor3f(0.5, 0.5, 0.5);
    glVertex2f(OFFSETS[0] - 3, CELLSIZE * HEIGHT + OFFSETS[3] + 3);
    glVertex2f(OFFSETS[0] - 3, CELLSIZE * HEIGHT + OFFSETS[3] - 3);
    glVertex2f(CELLSIZE * WIDTH + (OFFSETS[0] + 3) - ((CELLSIZE * HEIGHT + OFFSETS[3] + 3) - (CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2] - 3))), CELLSIZE * HEIGHT + OFFSETS[3] - 3);
    glVertex2f(CELLSIZE * WIDTH + (OFFSETS[0] + 3) - ((CELLSIZE * HEIGHT + OFFSETS[3] + 3) - (CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2] - 3))), CELLSIZE * HEIGHT + OFFSETS[3] + 3);
    
    glColor3f(0.756, 0.756, 0.756);
    glVertex2f(OFFSETS[0], CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2]));
    glVertex2f(CELLSIZE * WIDTH + OFFSETS[0], CELLSIZE * HEIGHT + (OFFSETS[2] + OFFSETS[2]));
    glVertex2f(CELLSIZE * WIDTH + OFFSETS[0], CELLSIZE * HEIGHT + OFFSETS[3]);
    glVertex2f(OFFSETS[0], CELLSIZE * HEIGHT + OFFSETS[3]);
    
    glEnd();
}

void drawClosedField(float x, float y, int pressed)
{
    if (pressed)
        glColor3f(1.0, 1.0, 1.0);
    else
        glColor3f(0.5, 0.5, 0.5);
    
    glBegin(GL_TRIANGLES);
    glVertex2f(OFFSETS[0] + x * CELLSIZE, OFFSETS[2] + y * CELLSIZE);
    glVertex2f(CELLSIZE + OFFSETS[0] + x * CELLSIZE, OFFSETS[2] + y * CELLSIZE);
    glVertex2f(CELLSIZE + OFFSETS[0] + x * CELLSIZE, CELLSIZE  + OFFSETS[2]+ y * CELLSIZE);
    
    if (pressed)
        glColor3f(0.5, 0.5, 0.5);
    else
        glColor3f(1.0, 1.0, 1.0);
    
    glVertex2f(OFFSETS[0] + x * CELLSIZE, OFFSETS[2] + y * CELLSIZE);
    glVertex2f(OFFSETS[0] + x * CELLSIZE, CELLSIZE + OFFSETS[2] + y * CELLSIZE);
    glVertex2f(CELLSIZE + OFFSETS[0] + x * CELLSIZE, CELLSIZE + OFFSETS[2] + y * CELLSIZE);
    glEnd();
    
    glColor3f(0.756, 0.756, 0.756);
    
    glBegin(GL_QUADS);
    glVertex2f(2 + OFFSETS[0] +  x * CELLSIZE, 2 + OFFSETS[2] + y * CELLSIZE);
    glVertex2f(CELLSIZE + OFFSETS[0] - 2 + x * CELLSIZE, 2 + OFFSETS[2] + y * CELLSIZE);
    glVertex2f(CELLSIZE + OFFSETS[0] - 2 + x * CELLSIZE, CELLSIZE - 2 + OFFSETS[2] + y * CELLSIZE);
    glVertex2f(2 + OFFSETS[0] + x * CELLSIZE, CELLSIZE - 2 + OFFSETS[2] + y * CELLSIZE);
    glEnd();
}

void drawOpenedField(float x, float y, int gameloose)
{
    if (gameloose)
        glColor3f(1, 0, 0);
    else
        glColor3f(0.756, 0.756, 0.756);
    glBegin(GL_QUADS);
    glVertex2f(x * CELLSIZE + OFFSETS[0], y * CELLSIZE + OFFSETS[2]);
    glVertex2f((x + 1) * CELLSIZE + OFFSETS[0], y * CELLSIZE + OFFSETS[2]);
    glVertex2f((x + 1) * CELLSIZE + OFFSETS[0], (y + 1) * CELLSIZE + OFFSETS[2]);
    glVertex2f(x * CELLSIZE + OFFSETS[0], (y + 1) * CELLSIZE + OFFSETS[2]);
    glEnd();
    
    glColor3f(0.5, 0.5, 0.5);
    glLineWidth(2.5);
    
    glBegin(GL_LINE_LOOP);
    glVertex2f(x * CELLSIZE + OFFSETS[0], y * CELLSIZE + OFFSETS[2]);
    
    glVertex2f(x * CELLSIZE + CELLSIZE + OFFSETS[0], y * CELLSIZE + OFFSETS[2]);
    
    glVertex2f(x * CELLSIZE + CELLSIZE + OFFSETS[0], y * CELLSIZE + CELLSIZE + OFFSETS[2]);
    
    glVertex2f(x * CELLSIZE + OFFSETS[0], y * CELLSIZE + CELLSIZE + OFFSETS[2]);
    
    glEnd();
}

void DrawRadioBox(int x, int y, int checked)
{
        glColor3f(0, 0, 0);
        DrawCircle(x, y, 6, 20, 0);
        if (checked)
            DrawCircle(x, y, 4, 20, 1);
}

void drawMine(float x, float y, int gameloose)
{
    float centerX = x * CELLSIZE + OFFSETS[0]+ CELLSIZE / 2;
    float centerY = y * CELLSIZE + OFFSETS[2]+ CELLSIZE / 2;
    
    drawOpenedField(x, y, gameloose);
    
    glColor3f(0.0f, 0.0f, 0.0f);
    
    DrawCircle(centerX, centerY, 5.5, 10, 1);
    
    glBegin(GL_LINES);
    glVertex2f(centerX - 8, centerY);
    glVertex2f(centerX + 8, centerY);
    
    glVertex2f(centerX, centerY - 8);
    glVertex2f(centerX, centerY + 8);
    
    glVertex2f(centerX - 6, centerY + 6);
    glVertex2f(centerX + 6, centerY - 6);
    
    glVertex2f(centerX + 6, centerY + 6);
    glVertex2f(centerX - 6, centerY - 6);
    glEnd();
    
    glColor3f(1, 1, 1);
    glBegin(GL_QUADS);
    glVertex2f(centerX - 1, centerY + 1);
    glVertex2f(centerX - 3, centerY + 1);
    
    glVertex2f(centerX - 3, centerY + 3);
    glVertex2f(centerX - 1, centerY + 3);

    glEnd();
}

void drawCellWithFlag(float x, float y, int error)
{
    if (error)
    {
        drawMine(x, y, 0);
        glLineWidth(1.5);
        glColor3f(1.0, 0.0, 0.0);
        glBegin(GL_LINES);
        glVertex2f(2 + OFFSETS[0] +  x * CELLSIZE, 2 + OFFSETS[2] + y * CELLSIZE);
        glVertex2f(CELLSIZE + OFFSETS[0] - 2 + x * CELLSIZE, CELLSIZE - 2 + OFFSETS[2] + y * CELLSIZE);
        glVertex2f(2 + OFFSETS[0] + x * CELLSIZE, CELLSIZE - 2 + OFFSETS[2] + y * CELLSIZE);
        glVertex2f(CELLSIZE + OFFSETS[0] - 2 + x * CELLSIZE, 2 + OFFSETS[2] + y * CELLSIZE);
        glEnd();
    } else
    {
        drawClosedField(x, y, 0);
        
        glColor3f(0.0, 0.0, 0.0);
        
        glBegin(GL_QUADS);
        glVertex2f(4 + OFFSETS[0] + x * CELLSIZE, 3 + OFFSETS[2] + y * CELLSIZE);
        glVertex2f(x * CELLSIZE + CELLSIZE - 4 + OFFSETS[0], 3 + OFFSETS[2] + y * CELLSIZE);
        glVertex2f(x * CELLSIZE + CELLSIZE - 4 + OFFSETS[0], 3 + OFFSETS[2] + y * CELLSIZE +  (CELLSIZE/6));
        glVertex2f(x * CELLSIZE + 4 + OFFSETS[0], 3 + OFFSETS[2] + y * CELLSIZE + (CELLSIZE/6));
        
        glVertex2f(7 + OFFSETS[0] + x * CELLSIZE, 3 + OFFSETS[2] + y * CELLSIZE);
        glVertex2f(x * CELLSIZE + CELLSIZE - 7 + OFFSETS[0], 3 + OFFSETS[2] + y * CELLSIZE);
        glVertex2f(x * CELLSIZE + CELLSIZE - 7 + OFFSETS[0], 3 + OFFSETS[2] + y * CELLSIZE +  (CELLSIZE/4));
        glVertex2f(x * CELLSIZE + 7 + OFFSETS[0], 3 + OFFSETS[2] + y * CELLSIZE +   (CELLSIZE/4));
        
        glVertex2f(10 + OFFSETS[0] + x * CELLSIZE, 3 + OFFSETS[2] + y * CELLSIZE);
        glVertex2f(x * CELLSIZE + CELLSIZE - 8 + OFFSETS[0], 3 + OFFSETS[2] + y * CELLSIZE);
        glVertex2f(x * CELLSIZE + CELLSIZE - 8 + OFFSETS[0], 3 + OFFSETS[2] + y * CELLSIZE +  (CELLSIZE/2));
        glVertex2f(x * CELLSIZE + 10 + OFFSETS[0], 3 + OFFSETS[2] + y * CELLSIZE + (CELLSIZE/2));
        
        glColor3f(1.0, 0.0, 0.0);
        
        glVertex2f(8 + OFFSETS[0] + x * CELLSIZE, y * CELLSIZE + (CELLSIZE/2) + OFFSETS[2]);
        glVertex2f(x * CELLSIZE + CELLSIZE - 8 + OFFSETS[0], y * CELLSIZE + (CELLSIZE/2) + OFFSETS[2]);
        glVertex2f(x * CELLSIZE + CELLSIZE - 8 + OFFSETS[0], y * CELLSIZE + CELLSIZE - 3 + OFFSETS[2]);
        glVertex2f(x * CELLSIZE + 8 + OFFSETS[0], y * CELLSIZE + CELLSIZE - 3 + OFFSETS[2]);
        
        glVertex2f(6 + OFFSETS[0] + x * CELLSIZE, y * CELLSIZE + (CELLSIZE/2) + 1 + OFFSETS[2]);
        glVertex2f(x * CELLSIZE + CELLSIZE - 10 + OFFSETS[0], y * CELLSIZE + (CELLSIZE/2) + 1 + OFFSETS[2]);
        glVertex2f(x * CELLSIZE + CELLSIZE - 10 + OFFSETS[0], y * CELLSIZE + CELLSIZE - 4 + OFFSETS[2]);
        glVertex2f(x * CELLSIZE + 6 + OFFSETS[0], y * CELLSIZE + CELLSIZE - 4 + OFFSETS[2]);
        
        glVertex2f(4 + OFFSETS[0] + x * CELLSIZE, y * CELLSIZE + (CELLSIZE/2) + 2 + OFFSETS[2]);
        glVertex2f(x * CELLSIZE + CELLSIZE - 12 + OFFSETS[0], y * CELLSIZE + (CELLSIZE/2) + 2 + OFFSETS[2]);
        glVertex2f(x * CELLSIZE + CELLSIZE - 12 + OFFSETS[0], y * CELLSIZE + CELLSIZE - 5 + OFFSETS[2]);
        glVertex2f(x * CELLSIZE + 4 + OFFSETS[0], y * CELLSIZE + CELLSIZE - 5 + OFFSETS[2]);
        
        glEnd();
        
    }
}

void drawOpenedFieldWithValues(float x, float y, int minesCount)
{
    drawOpenedField(x, y, 0);
    
    if (minesCount > 0)
    {
        switch (minesCount)
        {
            case 1:
                glColor3f(0.2588, 0.294, 1.0);
                break;
            case 2:
                glColor3f(0.0, 0.5411, 0.0);
                break;
            case 3:
                glColor3f(1.0, 0.0, 0.0);
                break;
            case 4:
                glColor3f(0.0, 0.7, 0.0);
                break;
            case 5:
                glColor3f(0.5, 0.4, 0.0);
                break;
            case 6:
                glColor3f(0.0, 0.8, 0.5);
                break;
            case 7:
                glColor3f(0.1, 0.1, 0.1);
                break;
            case 8:
                glColor3f(0.3, 0.3, 0.3);
                break;
        }
        glRasterPos2f((x * CELLSIZE) + 5.5 + OFFSETS[0], (y * CELLSIZE) + 3.75 + OFFSETS[2]);
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, '0' + minesCount);
    }
}