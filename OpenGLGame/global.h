#if defined (WIN32) || defined (WIN64)
	#include <GL/glut.h>
	#include <GL/gl.h>
#else
	#include <GLUT/glut.h>
    #include <OpenGL/gl.h>
#endif

#include <math.h>
#include <stdio.h>

#include <stdlib.h>
#include <time.h>

#define CELLSIZE 20
#define GLOBALOFFSET 23

extern int WIDTH, HEIGHT;
extern int MINESCOUNT;
extern int flagsCount;

static int OFFSETS[4] = {15, 15, 15, 70};
static enum State { CLOSED, OPENED, FLAG, CLICKED };
static enum SmileState { IDLE, WIN, LOSE, DOWN, WAITING };
static enum SettingsState { OPEN, CLOSE };

typedef struct
{
    enum State state;
    int hasMine;
} Cell;

extern int timerValue;