#include "global.h"
#include <stdlib.h>

void Sprint( int x, int y, char *st, void *font);

void drawMenuBar(enum SettingsState state);
void DrawButton(int x, int y, int width, int height, int border, char *caption, int fontOffsetX, int fontOffsetY, int clicked);
void drawFlagsCount();
void DrawCircle(float cx, float cy, float r, int num_segments, int solid);
void DrawArc(float cx, float cy, float r, float start_angle, float arc_angle, int num_segments);
void DrawSimpleSmileButton(float defaultX, float defaultY, int down);
void DrawSmileButton(enum SmileState state);
void drawBackground();
void drawClosedField(float x, float y, int pressed);
void drawOpenedField(float x, float y, int gameloose);
void DrawRadioBox(int x, int y, int checked);
void drawMine(float x, float y, int gameloose);
void drawCellWithFlag(float x, float y, int error);
void drawOpenedFieldWithValues(float x, float y, int minesCount);