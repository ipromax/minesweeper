#if defined (WIN32) || defined (WIN64)
#include "graphics.h"
#else
#include "graphics.c"
#endif

void randomizeMines(int count);
void initialization();
void reshape(int width, int height);
void Win();
void Loose();
void checkWin();
int getMinesAround(int i, int j);
void checkFieldsStatus();
void draw();
int getFlagsAround(int i, int j);
void openOtherFields();
void ClickSmileButton();
int CheckZone(int x, int y);
void CheckSettings(int x, int y, int clicked);
void mouse2(int button, int state, int x, int y);
void initializationSettings();
void drawSettings();
void OpenSettings();
void ResetState();
void mouse(int button, int state, int x, int y);
void timer(int x);
void resize(int width, int height);
void SetDefaultMode();